#ifndef CONFIG_H
#define CONFIG_H

const int width = 512;
const int height = 512;

const int threadsNumber = 512;
const int blocksNumber = 1024;

#endif
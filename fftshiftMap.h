#ifndef fftshift_map_h
#define fftshift_map_h


#include <iostream>
#include <cstdio>

using namespace std;

#include <cuda_runtime_api.h>

/** 
* Function fftshift_map maps coordinates (i,j) 
* to point (i_out, j_out) according to:
* http://www.mathworks.com/help/matlab/ref/fftshift.html
* ONLY FOR SAME DIMENSIONS MATRICES SIZExSIZE
*
* @param i - first coordinate, row of matrix
* @param j - second coordinate, column of matrix
* @param i_out - out param, mapped i coordinate
* @param j_out - out param, mapped j coordinate
* @param matrix_size - size of matrix, where coordinates are mapped 
*/
__device__ __host__
inline void fftshift_map( const int i, const int j, int &i_out, int &j_out, const int matrix_size){
	int half = matrix_size / 2;
	i_out =  i < half ? i + half : i - half;
	j_out = j < half ? j + half : j - half;	
}


#endif
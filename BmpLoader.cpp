#include "BmpLoader.hpp"

Color *load_bmp_from_file(const char *file_name, const int width, const int height){


	BITMAPINFO *bitmap_info = NULL;

	void *not_formated_data = LoadBitmap(file_name, &bitmap_info);

	
	if( !not_formated_data  ) return NULL;
	

	Color *color_data = new Color[width*height];
	for(int i=0; i < width*height; i++){
		int formated_data_idx = i*3;
		color_data[i].r = ((unsigned char*)not_formated_data)[formated_data_idx + 2];
		color_data[i].g = ((unsigned char*)not_formated_data)[formated_data_idx + 1];
		color_data[i].b = ((unsigned char*)not_formated_data)[formated_data_idx + 0];
		color_data[i].a = A_VALUE;
	}
	free(not_formated_data);
	return color_data;
}

void print_color_array(Color *array, const int width, const int height){
	for( int i =0 ; i < height*height; i++){
			printf("(r=%d, g=%d, b=%d)\n",array[i].r,array[i].g,array[i].b );
	}

}

void clean_color_array(Color *array)
{
	delete [] array;
}

void *LoadBitmap(const char *fileName, BITMAPINFO **info)
{
	FILE          *fp;        
	void          *bits;      
	unsigned long bitsize, infosize;  
	WORD    type;
	DWORD   size;
	WORD    reserved1;
	WORD    reserved2;
	DWORD   offBits;

	if (!(fp = fopen(fileName, "rb"))) {
		printf("Nie ma takiego pliku!");
		return NULL;
	}

	fread(&type, sizeof(WORD), 1, fp);
		if (type != BMP)
	{

	fclose(fp);
		return NULL;
	};

	fread(&size, sizeof(DWORD), 1, fp);
	fread(&reserved1, sizeof(WORD), 1, fp);
	fread(&reserved2, sizeof(WORD), 1, fp);
	fread(&offBits, sizeof(DWORD), 1, fp);

	infosize = offBits - 14;
	if (!(*info = (BITMAPINFO *)malloc(infosize)))
	{

		fclose(fp);
		return NULL;
	};

	if (fread(*info, 1, infosize, fp) < infosize)
	{

		free(*info);
		fclose(fp);
		return NULL;
	};


	if (!(bitsize = (*info)->bmiHeader.biSizeImage))
		bitsize = ((*info)->bmiHeader.biWidth * (*info)->bmiHeader.biBitCount + 7) /
		           8 * abs((*info)->bmiHeader.biHeight);

	if (!(bits = malloc(bitsize)))
	{
		free(*info);
		fclose(fp);
		return NULL;
	}

	if (fread(bits, 1, bitsize, fp) < bitsize)
	{
		free(*info);
		free(bits);
		fclose(fp);
		return NULL;
	};

	fclose(fp);
	return bits;
}

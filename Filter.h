#ifndef FILTER_H_
#define FILTER_H_


#include <cuda_runtime_api.h>
#include "Utils.h"
#include "Color.h"
#include "Config.h"
#include <math.h>
#include <cufft.h>
#include "fftshiftMap.h"
#include "helper_cuda.h"

void set_filter(
	unsigned char *dev_R_matrix,
	unsigned char *dev_G_matrix,
	unsigned char *dev_B_matrix,
	const int dimension,
	const int spectrum	);


/**
* API 
*/

void convert_color_cuda_array_to_rgb_on_cuda(
	cudaArray* dev_colorsArray,
	unsigned char **out_dev_R_matrix,
	unsigned char **out_dev_G_matrix,
	unsigned char **out_dev_B_matrix,
	const int dimension 	
);

void convert_rbg_to_color_cuda_array_on_cuda(
	cudaArray* out_dev_colorsArray,
	unsigned char *dev_R_matrix,
	unsigned char *dev_G_matrix,
	unsigned char *dev_B_matrix,
	const int dimension 	
);



/**
* API END
*/


__device__
static
void convert_color_vector_to_rgb_matrices(
	unsigned char * dev_colorsArray,
	unsigned char *out_dev_R_matrix,
	unsigned char *out_dev_G_matrix,
	unsigned char *out_dev_B_matrix,
	const int dimension 
);

__device__
static
void convert_rgb_matrices_to_color_vector(
	unsigned char * out_dev_colorsArray,
	unsigned char *dev_R_matrix,
	unsigned char *dev_G_matrix,
	unsigned char *dev_B_matrix,
	const int dimension 
);

__global__
static
void kernel_convert_color_cuda_array_to_rgb_on_cuda(
	unsigned char * dev_colorsArray,
	unsigned char *out_dev_R_matrix,
	unsigned char *out_dev_G_matrix,
	unsigned char *out_dev_B_matrix,
	const int dimension );

__global__
static
void kernel_convert_rbg_to_color_cuda_array_on_cuda(
	unsigned char *out_dev_colorsArray,
	unsigned char *dev_R_matrix,
	unsigned char *dev_G_matrix,
	unsigned char *dev_B_matrix,
	const int dimension );

__global__
static
void kernel_set_filter(
	unsigned char *dev_R_matrix,
	unsigned char *dev_G_matrix,
	unsigned char *dev_B_matrix,
	const int dimension,
	const int spectrum	);


static
void set_filter_on_matrix(unsigned char *matrix, const int dimension, const int spectrum);


#endif
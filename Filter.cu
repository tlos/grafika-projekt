#include "Filter.h"

void set_filter(
	unsigned char *dev_R_matrix,
	unsigned char *dev_G_matrix,
	unsigned char *dev_B_matrix,
	const int dimension,
	const int spectrum	)
{
	//kernel_set_filter<<<blocksNumber,threadsNumber>>>(dev_R_matrix, dev_G_matrix, dev_B_matrix, dimension, spectrum);
	set_filter_on_matrix( dev_R_matrix, dimension, spectrum );
	set_filter_on_matrix( dev_G_matrix, dimension, spectrum );
	set_filter_on_matrix( dev_B_matrix, dimension, spectrum );
}

void convert_rbg_to_color_cuda_array_on_cuda(
	cudaArray* out_dev_colorsArray,		
	unsigned char *dev_R_matrix,
	unsigned char *dev_G_matrix,
	unsigned char *dev_B_matrix,
	const int dimension 	
){
	printf("convert_rbg_to_color_cuda_array_on_cuda\n");
	unsigned char *memDevTmp;
	int size = dimension*dimension*sizeof(Color);
	HANDLE_ERROR( cudaMalloc((void**)&memDevTmp, size ) );
	kernel_convert_rbg_to_color_cuda_array_on_cuda<<<blocksNumber,threadsNumber>>>
		(memDevTmp, dev_R_matrix, dev_G_matrix, dev_B_matrix, dimension);
	
	HANDLE_ERROR( cudaMemcpyToArray(out_dev_colorsArray, 0,0,memDevTmp, size, cudaMemcpyDeviceToDevice) );
	cudaDeviceSynchronize();
	cudaFree(memDevTmp);
}


void convert_color_cuda_array_to_rgb_on_cuda(
	cudaArray* dev_colorsArray,
	unsigned char **out_dev_R_matrix,
	unsigned char **out_dev_G_matrix,
	unsigned char **out_dev_B_matrix,
	const int dimension 	
){
	printf("convert_color_cuda_array_to_rgb_on_cuda\n");
	unsigned char *memDevTmp;
	int size = dimension*dimension*sizeof(Color);
	HANDLE_ERROR( cudaMalloc((void**)&memDevTmp, size ) );
	HANDLE_ERROR( cudaMemcpyFromArray(memDevTmp,dev_colorsArray, 0,0,size,cudaMemcpyDeviceToDevice) );
	

	size = dimension*dimension;
	HANDLE_ERROR( cudaMalloc((void**)out_dev_R_matrix, size) );
	HANDLE_ERROR( cudaMalloc((void**)out_dev_G_matrix, size) );
	HANDLE_ERROR( cudaMalloc((void**)out_dev_B_matrix, size) );

	kernel_convert_color_cuda_array_to_rgb_on_cuda<<<blocksNumber,threadsNumber>>>
		(memDevTmp, *out_dev_R_matrix, *out_dev_G_matrix, *out_dev_B_matrix, dimension);
	cudaDeviceSynchronize();
	cudaFree( memDevTmp );
}





__device__
static
void convert_color_vector_to_rgb_matrices(
	unsigned char * dev_colorsArray,
	unsigned char *out_dev_R_matrix,
	unsigned char *out_dev_G_matrix,
	unsigned char *out_dev_B_matrix,
	const int dimension 
){
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	int offset = x + y * blockDim.x * gridDim.x;
	long long dim2 = dimension*dimension;
	if( offset < dim2 ){
		int new_offset = offset * 4;
		out_dev_R_matrix[offset] = dev_colorsArray[new_offset + 0];
		out_dev_G_matrix[offset] = dev_colorsArray[new_offset + 1];
		out_dev_B_matrix[offset] = dev_colorsArray[new_offset + 2];
	}
}



__device__
static
void convert_rgb_matrices_to_color_vector(
	unsigned char * out_dev_colorsArray,
	unsigned char *dev_R_matrix,
	unsigned char *dev_G_matrix,
	unsigned char *dev_B_matrix,
	const int dimension 
){
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	int offset = x + y * blockDim.x * gridDim.x;
	long long dim2 = dimension*dimension;
	if( offset < dim2 ){
		int new_offset = offset * 4;
		out_dev_colorsArray[new_offset + 0] = dev_R_matrix[offset];
		out_dev_colorsArray[new_offset + 1] = dev_G_matrix[offset];
		out_dev_colorsArray[new_offset + 2] = dev_B_matrix[offset];
	}
}




__global__
static
void kernel_convert_rbg_to_color_cuda_array_on_cuda(
	unsigned char *out_dev_colorsArray,
	unsigned char *dev_R_matrix,
	unsigned char *dev_G_matrix,
	unsigned char *dev_B_matrix,
	const int dimension 	)
{
	convert_rgb_matrices_to_color_vector(out_dev_colorsArray,dev_R_matrix, dev_G_matrix, dev_B_matrix,dimension);
}

__global__
static
void kernel_convert_color_cuda_array_to_rgb_on_cuda(
	unsigned char * dev_colorsArray,
	unsigned char *out_dev_R_matrix,
	unsigned char *out_dev_G_matrix,
	unsigned char *out_dev_B_matrix,
	const int dimension )
{
	convert_color_vector_to_rgb_matrices(dev_colorsArray, out_dev_R_matrix,out_dev_G_matrix, out_dev_B_matrix, dimension);
}

__global__
static
void kernel_set_filter(
	unsigned char *dev_R_matrix,
	unsigned char *dev_G_matrix,
	unsigned char *dev_B_matrix,
	const int dimension,
	const int spectrum	)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	int offset = x + y * blockDim.x * gridDim.x;
	long long size = dimension*dimension;
	if( offset < size )
	{
		dev_R_matrix[offset] = dev_B_matrix[offset];
	}
}

__global__ 
static
void copy_matrix_to_real(unsigned char *matrix, cufftReal *real_in_matrix, long long size)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	int offset = x + y * blockDim.x * gridDim.x;
	if( offset < size )
	{
		real_in_matrix[offset] = matrix[offset];
	}
}


__global__ 
static
void copy_matrix_from_real(unsigned char *matrix, cufftReal *real_in_matrix, long long size)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	int offset = x + y * blockDim.x * gridDim.x;
	if( offset < size )
	{
		matrix[offset] = (unsigned char)(real_in_matrix[offset]/(float)size);
		//complex_in_matrix[offset].x = matrix[offset];
		//complex_in_matrix[offset].y = 0;
	}
}

static void printf_real_matrix(cufftReal *dev_matrix, long long size)
{
	cufftReal *host_matrix = (cufftReal*)malloc(size*sizeof(cufftReal));
	//HANDLE_ERROR( cudaMalloc((void**)&host_matrix, size*sizeof(cufftReal)) );
	HANDLE_ERROR( cudaMemcpy(host_matrix,dev_matrix, size*sizeof(cufftReal), cudaMemcpyDeviceToHost) );
	printf("$$$$$$$$$$$$$$$$\nMatrix:\n");

	size = 10;

	for(unsigned int i =0 ; i < size; i++){
		printf("dev_matrix[%d] = %f\n",i,host_matrix[i]);
	}
	printf("$$$$$$$$$$$$$$$$\nEND\n");

	free(host_matrix);
}

__global__ void kernel_set_zeros(cufftComplex *complex_in_matrix, const int spectrum, const int dimension)
{
	int offset = (threadIdx.x + blockIdx.x * blockDim.x) + 
		(threadIdx.y + blockIdx.y * blockDim.y)
		* blockDim.x * gridDim.x;
	if( offset < dimension*dimension){
		int half_dim = dimension / 2;
		int x = offset % dimension;
		int y = offset / dimension;
		if( half_dim + spectrum <= x || x < half_dim - spectrum  ){
			if( half_dim + spectrum <= y || y < half_dim - spectrum  ){
				cufftComplex c;
				//map
				int x2,y2;
				fftshift_map(x,y,x2,y2,dimension);
				c.x = 0;
				c.y = 0;
				complex_in_matrix[ x2*dimension + y2] = c;
			}
		}
	}


}


static
void set_filter_on_matrix(unsigned char *matrix, const int dimension, const int spectrum)
{
	printf("set_filter_on_matrix\n");
	long long size = dimension*dimension;
	cufftComplex *complex_in_matrix;
	cufftReal *real_in_matrix;
	HANDLE_ERROR( cudaMalloc((void**)&complex_in_matrix, size*sizeof(cufftComplex)) );
	HANDLE_ERROR( cudaMalloc((void**)&real_in_matrix, size*sizeof(cufftReal)) );

	copy_matrix_to_real<<<blocksNumber,threadsNumber>>> ( matrix, real_in_matrix, size );
	//printf_real_matrix(real_in_matrix,size);
	cudaDeviceSynchronize();


    cufftHandle plan;
	checkCudaErrors(cufftPlan2d(&plan, dimension, dimension, CUFFT_R2C));
	
    printf("Transforming signal cufftExecC2C\n");
    checkCudaErrors(
		cufftExecR2C(plan, real_in_matrix, complex_in_matrix)
		);
	cudaDeviceSynchronize();
	


	kernel_set_zeros<<< blocksNumber, threadsNumber >>>(complex_in_matrix, spectrum, dimension);
	cudaDeviceSynchronize();

	cufftDestroy(plan);
	checkCudaErrors(cufftPlan2d(&plan, dimension, dimension, CUFFT_C2R));
    printf("Transforming signal back cufftExecC2C\n");
    checkCudaErrors(
		cufftExecC2R(plan, complex_in_matrix, real_in_matrix)
		);
	cudaDeviceSynchronize();
	
	//printf_real_matrix(real_in_matrix,size);

	copy_matrix_from_real<<<blocksNumber,threadsNumber>>> ( matrix, real_in_matrix, size );
	cudaDeviceSynchronize();

	cufftDestroy(plan);

	cudaFree(complex_in_matrix);
}
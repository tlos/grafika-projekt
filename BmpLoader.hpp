#ifndef __BMP__LOADER
#define __BMP__LOADER


#include <stdio.h>
#include "Color.h"

#define PUBLIC
#define PRIVATE static

	

/******************
*
* 	API 
*
******************/


	/**
	* Loades BMP file "file_name" with sieze (width)x(height) to Color array.
	* Color array created by new keyword. After usage should release it with 
	* clean_color_array 
	*/
	PUBLIC Color *load_bmp_from_file(const char *file_name, const int width, const int height);
	PUBLIC void clean_color_array(Color *array);
	PUBLIC void print_color_array(Color *array, const int width, const int height);
	#define A_VALUE 255


/******************
*
* 	API  END
*
******************/




#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>

#define BMP              19778


/*
#define BI_RGB        0L
#define BI_RLE8       1L
#define BI_RLE4       2L
#define BI_BITFIELDS  3L
typedef GLubyte       BYTE;
typedef GLushort      WORD;
typedef GLuint        DWORD;
typedef GLint         LONG;

typedef struct tagRGBQUAD {
        BYTE    rgbBlue;
        BYTE    rgbGreen;
        BYTE    rgbRed;
        BYTE    rgbReserved;
} RGBQUAD;

typedef struct tagBITMAPINFOHEADER{
        DWORD      biSize;
        LONG       biWidth;
        LONG       biHeight;
        WORD       biPlanes;
        WORD       biBitCount;
        DWORD      biCompression;
        DWORD      biSizeImage;
        LONG       biXPelsPerMeter;
        LONG       biYPelsPerMeter;
        DWORD      biClrUsed;
        DWORD      biClrImportant;
} BITMAPINFOHEADER;

typedef struct tagBITMAPINFO {
    BITMAPINFOHEADER    bmiHeader;
    RGBQUAD             bmiColors[1];
} BITMAPINFO;

typedef struct tagBITMAPFILEHEADER {
        WORD    bfType;
        DWORD   bfSize;
        WORD    bfReserved1;
        WORD    bfReserved2;
        DWORD   bfOffBits;
} BITMAPFILEHEADER;*/

//BITMAPINFO	*BitmapInfo = NULL;	/* Current bitmap information */
//void		*BitmapBits = NULL;	/* Current bitmap pixel bits */

void *LoadBitmap(const char *, BITMAPINFO **);
GLubyte *ConvertRGB(BITMAPINFO *, void*);


#endif 

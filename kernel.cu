#define _CRT_SECURE_NO_WARNINGS


/**
* Tomasz �o�
* Filtracja dolnoprzepustowa obraz�w
* 08.2013 - Grafika Komputerowa
*/

#include "Config.h"
#include <stdio.h>
#include <string.h>
#include <GL/glew.h>
#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>
#include "Utils.h"
#include "BmpLoader.hpp"
#include "fftshiftMap.h"
#include "Filter.h"

#pragma comment(lib, "glew32.lib")

/* FILE */
const char * IMAGE_FILE_NAME = "d:/img2.bmp";

/* GLOBALS */
int spectrum = 250; /*max 255, half of img*/	

cudaArray* memDevice;
unsigned char *dev_R_matrix;
unsigned char *dev_G_matrix;
unsigned char *dev_B_matrix;

/* Handles OpenGL-CUDA exchange. */
cudaGraphicsResource *cuda_texture;


void updateMemDevice(cudaArray *memDevice, int w, int h);
void run_filtering();


/* Registers (+ resizes) CUDA-Texture (aka Renderbuffer). */
void resizeTexture(int w, int h) {
    static GLuint gl_texture = 0;

    /* Delete old CUDA-Texture. */
    if (gl_texture) {
        cudaGraphicsUnregisterResource(cuda_texture);
        glDeleteTextures(1, &gl_texture);
    } else glEnable(GL_TEXTURE_2D);

    glGenTextures(1, &gl_texture);
    glBindTexture(GL_TEXTURE_2D, gl_texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

    cudaGraphicsGLRegisterImage(&cuda_texture, gl_texture, GL_TEXTURE_2D, cudaGraphicsMapFlagsWriteDiscard);
}


void run_filtering(){
	convert_color_cuda_array_to_rgb_on_cuda(memDevice, &dev_R_matrix, &dev_G_matrix, &dev_B_matrix, width);
	set_filter( dev_R_matrix, dev_G_matrix, dev_B_matrix, width, spectrum );
	convert_rbg_to_color_cuda_array_on_cuda(memDevice, dev_R_matrix, dev_G_matrix, dev_B_matrix,width);
}

void updateMemDevice(cudaArray *memDevice, int w, int h) {
    Color *memHost = load_bmp_from_file(IMAGE_FILE_NAME,w,h);
    cudaMemcpyToArray(memDevice, 0, 0, memHost, w*h*4, cudaMemcpyHostToDevice);
    delete [] memHost;
}

void editTexture(int w, int h) {
    cudaGraphicsMapResources(1, &cuda_texture);
    cudaGraphicsSubResourceGetMappedArray(&memDevice, cuda_texture, 0, 0);
    updateMemDevice(memDevice, w, h);
    cudaGraphicsUnmapResources(1, &cuda_texture);
}

int curW = width, curH = height;
void windowResizeFunc(int w, int h) {
	curW = w;
	curH = h;
    glViewport(-w, -h, w*2, h*2);
}

void displayFunc() {
    glBegin(GL_QUADS);
    glTexCoord2i(0, 0); glVertex2i(0, 0);
    glTexCoord2i(1, 0); glVertex2i(1, 0);
    glTexCoord2i(1, 1); glVertex2i(1, 1);
    glTexCoord2i(0, 1); glVertex2i(0, 1);
    glEnd();

    glFlush();
}

//#include <Windows.h>
void keyboard(unsigned char key, int x, int y){
	switch (key) {
		case 'q':
        case 27:
            exit(0);
		case 'f':
			run_filtering();
			glutPostRedisplay();
			break;
		case 'r':
			//updateMemDevice(memDevice, width, height);
			editTexture( width, height);
			glutPostRedisplay();
			break;
		case '+':
			if( spectrum < 254 ){
				printf("Spectrum += 2\n");
				spectrum += 2;
			}
			printf("Spectrum= %d\n",spectrum);
			break;
		case '-':
			if( spectrum > 0 ){
				printf("Spectrum -= 2\n");
				spectrum -= 2;
			}
			printf("Spectrum= %d\n",spectrum);
			break;
    }
}
int main(int argc, char *argv[]) {
    /* Initialize OpenGL context. */
	

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB);
    glutInitWindowSize(width, height);
    glutCreateWindow("Bitmap in Device Memory");
    glutReshapeFunc(windowResizeFunc);
    glutDisplayFunc(displayFunc);
	glutKeyboardFunc(keyboard);
    glewInit();

    cudaGLSetGLDevice(0);

    resizeTexture(width, height);
    editTexture(width, height);
    glutMainLoop();
    return 0;
}